package com.buutcamp.dependencyinjection.cages;

import com.buutcamp.dependencyinjection.animals.Animal;

public interface Cage {

    void setAnimal(Animal animal);


    String getAnimal();

}
