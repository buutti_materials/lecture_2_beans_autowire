package com.buutcamp.dependencyinjection.cages;

import com.buutcamp.dependencyinjection.animals.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GlassCage implements Cage {

    @Autowired
    @Qualifier("getMouse")
    private Animal animal;

    public void setAnimal(Animal animal) {
        this.animal=animal;
    }

    public String getAnimal() {
        return animal.getName();
    }

    public GlassCage() {
    }





}
