package com.buutcamp.dependencyinjection.cages;

import com.buutcamp.dependencyinjection.animals.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class FencedCage implements Cage {

    private Animal animal;

    @Autowired
    public FencedCage(@Qualifier("getCat") Animal animal) {
        this.animal = animal;
    }


    public void setAnimal(Animal animal) {

    }

    public String getAnimal() {
        return animal.getName();
    }


}
