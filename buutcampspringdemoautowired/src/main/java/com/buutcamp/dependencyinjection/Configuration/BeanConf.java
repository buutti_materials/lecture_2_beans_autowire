package com.buutcamp.dependencyinjection.Configuration;

import com.buutcamp.dependencyinjection.animals.Animal;
import com.buutcamp.dependencyinjection.animals.Cat;
import com.buutcamp.dependencyinjection.animals.Mouse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"com.buutcamp.dependencyinjection"})
public class BeanConf {

    @Bean
    public Animal getCat() {
        return new Cat();
    }

    @Bean
    public Animal getMouse() {
        return new Mouse();
    }
}
