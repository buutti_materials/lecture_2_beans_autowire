package com.buutcamp.dependencyinjection.main;

import com.buutcamp.dependencyinjection.Configuration.BeanConf;
import com.buutcamp.dependencyinjection.cages.Cage;
import com.buutcamp.dependencyinjection.cages.GlassCage;
import com.buutcamp.dependencyinjection.cages.FencedCage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class RunApp {

    public RunApp() {

        AnnotationConfigApplicationContext appCtx = new AnnotationConfigApplicationContext(BeanConf.class);

        Cage glassCage = appCtx.getBean(GlassCage.class);
        Cage fencedCage = appCtx.getBean(FencedCage.class);

        System.out.println(glassCage.getAnimal());
        System.out.println(fencedCage.getAnimal());

        appCtx.close();
    }
}
