package com.buutcamp.dependencyinjection.animals;


public class Cat implements Animal {

    private String name = "Cat, Harry";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
