package com.buutcamp.dependencyinjection.animals;

public interface Animal {

    public String getName();
    public void setName(String str);


}
