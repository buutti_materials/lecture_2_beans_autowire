package com.buutcamp.dependencyinjection.animals;


public class Mouse implements Animal {

    private String name = "Mouse, Larry";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
